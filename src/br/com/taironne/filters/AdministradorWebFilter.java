package br.com.taironne.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter(filterName = "AdministradorFilter", urlPatterns = {"/administrador/*"}) 
public class AdministradorWebFilter implements Filter{
	
    public void init(FilterConfig config){
    } 
    
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain fc) throws IOException, ServletException{
    	fc.doFilter(req, res);
    	System.out.println("Fitrando Administrador!");
    }
    
    public void destroy(){
	}
}
