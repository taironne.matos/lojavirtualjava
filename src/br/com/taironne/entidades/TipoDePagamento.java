package br.com.taironne.entidades;

public enum TipoDePagamento {
	
	CARTAO(1),
	
	BOLETO(2),
	
	DEPOSITO(3); 
	
	private int codigo;
	
	TipoDePagamento (int code) {
		this.codigo = code;
	}
	
	public int getCode() {
		return codigo;
	}
	
}
