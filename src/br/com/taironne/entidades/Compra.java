package br.com.taironne.entidades;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Compra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long codigo;
	
	@OneToMany
	private ArrayList<ItemCompra> itens;
	
	@OneToOne
	private Cliente cliente;
	
	@Temporal(TemporalType.DATE)
	private Date dataCompra = new java.sql.Date(System.currentTimeMillis());
	
	private TipoDePagamento tipoPagamento;

	public Compra() {}
	
	public Compra(Long codigo, ArrayList<ItemCompra> itens, Cliente cliente, Date dataCompra) {
		super();
		this.codigo = codigo;
		this.itens = itens;
		this.cliente = cliente;
		this.dataCompra = dataCompra;
	}

	public ArrayList<ItemCompra> getItens() {
		return itens;
	}

	public void setItens(ArrayList<ItemCompra> itens) {
		this.itens = itens;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Long getCodigo() {
		return codigo;
	}

	public TipoDePagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoDePagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}	
	
}
