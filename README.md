# Projeto Loja Virtual Java
 Este é um projeto de estudo desenvolvido para a cadeira de Programação Avançada para Web do cuso de Sistemas Para Internet UNIPÊ
 
## Conteúdo
 Atualmente o sistema conta com a implementação das seguintes classes:
* **Administrador**
* **Cliente**
* **Compra**
* **Endreço**
* **Estoque**
* **ItemCompra**
* **Pessoa**
* **Produto**
* **TipoDePagamento**
 
 As classes já tem os seus endpoints implementados para os quatro verbos CRUD Webservlet HTTP (GET, POST, PUT, DELETE) e WebFilter.

## Autores

* **Alexandre Silva dos Santos** 
* **Anibal de Medeiros Batista Filho** 
* **Ronaldo Costa Cordeiro** 
* **Rodrigo Oliveira Gomes dos Santos** 
* **Marcel Marques** [Github](https://github.com/marcmatias)
* **Taironne Darley Torres Matos**